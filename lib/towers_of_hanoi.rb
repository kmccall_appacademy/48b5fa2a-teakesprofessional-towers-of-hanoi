# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_accessor :game, :towers
  
  def initialize
    self.towers = [[3, 2, 1], [], []]
    @num = 0
  end

  def play
    puts "Towers of Hanoi --- Numbers are disks, move the disks from tower to tower one at a time until you can reconstruct the tower"
    self.render
    until self.won?
      puts "Please choose a tower to take from (1-3)..."
      from = gets.chomp.to_i - 1
      puts "Where would you like to move the selected disk?"
      to = gets.chomp.to_i - 1
      move(from, to)
      self.render
    end
  end
  
  def render
    p self.towers
  end
  
  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      disk = self.towers[from_tower].pop
      self.towers[to_tower].push(disk)
      self.won?
      @num += 1
    else
      return puts "INVALID MOVE! PLEASE DO NOT STACK LARGER DISKS ON SMALLER DISKS"
    end
    
  end
  
  def valid_move?(from_tower, to_tower)
    return false if !(0..2).include?(from_tower) || !(0..2).include?(to_tower)
    return false if self.towers[from_tower].empty?
    unless self.towers[to_tower].empty?
      return self.towers[from_tower].last < self.towers[to_tower].last 
    end
    true
  end
  
  def won?
    win_condition = [3, 2, 1]
      unless @num == 0

          if towers[1] == win_condition || towers[2] == win_condition
            puts "You Win!!!"
            return true
          end
      end
    false
  end
end